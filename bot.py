#!/usr/local/src/ds181bot/venv/bin/python
# -*- coding: utf-8 -*-

import config  
import os
import re
import requests
from bs4 import BeautifulSoup
import datetime


###### Функция вывода готового поста для телеграм-бота
def output(news, i):
    news = news.find('div', {'class': 'art-postmetadatafooter'}) ## находим элемент div соответствующего класса
    notice = news.find('a') ## ищем в нём элемент <a>
    link = notice.get('href') ## выдергиваем из <a> ссылку
    link = link.replace('http://xn--181-mdd9d.xn--p1ai','') ## убираем доменное имя сайта из ссылки
    link = '(http://xn--181-mdd9d.xn--p1ai' + link +')' ## переводим ссылку в синтаксис телеграмма
    title = '[' + notice.get('title') + ']' ## переводим наименование ссылки в синтаксис телеграмма
    text = '*Опубликован новый пост № ' + str(i) + '* \n' + title + link
    return text

###### Функция проверки наличия файла, возвращает id последнего запомненного поста
def checkFile(file):
    ## проверяем существует ли файл, выцепляем значение z либо z = 0
    ## z хранит значение
    if os.path.exists(file): 
        f = open(file, 'r')
        z = f.read()
        if z == '':
            z = 0
    else:
        f = open(file, 'w')
        z = 0
        f.close()
    z = int(z)
    return z


## Начинаем парсить сайт дс181.рф, ищем блок div, выцепляем у него id_div
res = requests.get('http://xn--181-mdd9d.xn--p1ai/')
html = res.text
soup = BeautifulSoup(html, 'lxml') ##### обязательно сделать pip3 install lxml перед запуском!!!
div = soup.find('div', {'class': 'node node-news node-promoted node-teaser'})
id_div = int(div.get('id').replace('node-',''))

## Файлы на сервере
reportfile = '/root/ds181bot/report.txt'
chkfile = '/root/ds181bot/text.txt'
## Данные для отравки в Телеграм
group = config.GROUP
linkTelegram = config.link
## Получаем текущие дату и время
today = datetime.datetime.today()
#today = today.strftime("%d.%m.%Y %H:%M:%S")
today = today.strftime("%d.%m.%Y %H:%M")

   
## Выверяем id поста 
y = checkFile(chkfile)
if id_div > y:
    newPost = output(div, id_div) 
    y = id_div
    file = open(chkfile,'w') # записываем в text.txt id новости
    file.write(str(y))
    file.close()
    data = {
      'chat_id': group,
      'text': newPost,
      'parse_mode' : 'Markdown'
    }
    response = requests.post(linkTelegram, data=data) # Отправляем в телегу уведомление
    newPostRep = today + ' new post #' + str(y) + '\n' # Фиксируем в отчете report.txt запись о том, что есть новая запись
    report = open(reportfile, 'a')
    report.write(str(newPostRep))
    report.close()
else: 
    noNews = today + ' no new posts\n'  # Фиксируем в отчете report.txt запись о том, что новостей нет
    if os.path.exists(reportfile):  # Проверяем существует ли файл
        report = open(reportfile, 'a')
        report.write(str(noNews))
        report.close()
    else:
        report = open(reportfile, 'w')
        report.write(str(noNews))
        report.close()

exit()



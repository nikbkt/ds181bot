# DS181 TELEGRAM BOT  
## Бот для парсинга сайта дс181.рф на наличие новых постов и отправки их в чат телеграм посредством бота  
##  Инструкция по установке на Centos7  

### Устанавливаем python 3 версии   
`yum update -y`   
`yum install epel-release -y`  
`yum install python3 -y`  

### Устанавливаем git и настраиваем его   
`yum install git -y`   
`git config --global user.name "Ваше имя"`   
`git config --global user.email "youremail@yourdomain.com"`  

### Переходим в директорию, где будет располагаться бот (в примере /usr/local/src)   
`cd /usr/local/src`   

### Клонируем проект в директорию и переходим в ds181bot   
`git clone https://gitlab.com/d_nikolaev/ds181bot.git`   
`cd ./ds181bot/`   

### Делаем файл install.sh исполняемым   
`sudo chmod +x install.sh`   

### Запускаем install.sh   
`./install.sh`   

### Если проект склонирован в другую директорию (не в /usr/local/src), необходимо изменить путь до виртуального окружения в файле bot.py   
в первой строке скрипта прописать:   
`#!/<YOUR_PATH>/ds181bot/venv/bin/python`   
где <YOUR_PATH> - директория, в которую вы склонировали проект

### Открываем на редактирование файл config.py и подставляем свои значения TOKEN и ID_GROUP, сохраняем   
`link = 'https://api.telegram.org/bot<TOKEN>/sendMessage'`   
`GROUP = '<ID_GROUP>'`   

### Открываем задания cron   
`crontab -e`   

### Добавляем новое задание   
`0 9,11,13,15,17,19 * * * source /root/ds181bot/venv/bin/activate && /root/ds181bot/bot.py >> /var/log/ds181bot.log 2> /var/log/ds181bot-error.log`   
Бот будет запускаться каждый день в 9, 11, 13, 15, 17 и 19 часов

### Обязательно перезапускаем cron   
`systemctl restart crond`   